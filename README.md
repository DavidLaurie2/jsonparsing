# JSON Parsing

This uses the [Parle](https://www.php.net/manual/en/book.parle.php) library which has to be installed via pecl.

`pecl install parle-beta`

It requires a C++14 compatible compiler to be installed on the system.

I implemented a JSON parser in it, and then extended it to support trailing commas and comments.

[I found this helpful when figuring out how to use the library.](https://en.wikipedia.org/wiki/Shift-reduce_parser)