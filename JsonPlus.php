<?php

use Parle\Lexer;
use Parle\Parser;
use Parle\ParserException;
use Parle\Token;
use Parle\Stack;

class JsonPlus
{
    private Parser $parser;
    private Lexer $lexer;
    private array $reducers = [];

    public function __construct()
    {
        $this->parser = new Parser;
        $this->initParserTokens($this->parser);
        $this->initParserRules($this->parser);
        $this->parser->build();

        $this->lexer = new Lexer;
        $this->initLexer($this->lexer, $this->parser);
        $this->lexer->build();
    }

    /**
     * @param string $json
     * @return mixed
     * @throws ParserException
     */
    public function decode(string $json, bool $debugPrint = false)
    {
        $this->parser->consume($json, $this->lexer);
        $stack = new Stack;

        while (true) {
            switch ($this->parser->action) {
                case Parser::ACTION_ACCEPT:
                    break 2;
                case Parser::ACTION_ERROR:
                    $err = $this->parser->errorInfo();
                    if (Parser::ERROR_UNKNOWN_TOKEN == $err->id) {
                        $tok = $err->token;
                        $msg = "Unknown token '{$tok->value}' at offset {$err->position}";
                    } else if (Parser::ERROR_NON_ASSOCIATIVE == $err->id) {
                        $tok = $err->token;
                        $msg = "Token '{$tok->id}' at offset {$this->lexer->marker} is not associative";
                    } else if (Parser::ERROR_SYNTAX == $err->id) {
                        $msg = "Syntax error at offset {$this->lexer->marker}";
                    } else {
                        $msg = "Parse error";
                    }
                    throw new ParserException($msg);
                case Parser::ACTION_REDUCE:
                    $this->reducers[$this->parser->reduceId]($this->parser, $stack);
                    break;
                case Parser::ACTION_SHIFT:
                case Parser::ACTION_GOTO:
                default:
                    break;
            }

            if ($debugPrint) {
                print "{$this->parser->trace()}\n";
            }

            $this->parser->advance();
        }

        return $stack->top;
    }

    private function addRule(string $name, string $rule, callable $reducer)
    {
        $id = $this->parser->push($name, $rule);
        $this->reducers[$id] = $reducer;
    }

    private function parseString($string) : string
    {
        $string = substr($string, 1, -1);
        $replaces = [
            "\\\"" => "\"",
            "\\\n" => "\n",
        ];
        return str_replace(array_keys($replaces), array_values($replaces), $string);
    }

    private function initParserTokens(Parser $parser)
    {
        $parser->token('open_brace');
        $parser->token('close_brace');
        $parser->token('open_bracket');
        $parser->token('close_bracket');
        $parser->token('colon');
        $parser->token('comma');
        $parser->token('value_string');
        $parser->token('value_float');
        $parser->token('value_integer');
        $parser->token('value_true');
        $parser->token('value_false');
        $parser->token('value_null');
    }

    private function initLexer(Lexer $lexer, Parser $parser)
    {
        $lexer->push('\{', $parser->tokenId('open_brace'));
        $lexer->push('\}', $parser->tokenId('close_brace'));
        $lexer->push('\[', $parser->tokenId('open_bracket'));
        $lexer->push('\]', $parser->tokenId('close_bracket'));
        $lexer->push(':', $parser->tokenId('colon'));
        $lexer->push(',', $parser->tokenId('comma'));
        $lexer->push('\"([^\"\\\\]|\\\\\")*\"', $parser->tokenId('value_string'));
        $lexer->push('[0-9]+', $parser->tokenId('value_integer'));
        $lexer->push('[0-9]+\\.[0-9]+((E|e)-?[0-9]+)?', $parser->tokenId('value_float'));
        $lexer->push('[0-9]+((E|e)-?[0-9]+)?', $parser->tokenId('value_float'));
        $lexer->push('true', $parser->tokenId('value_true'));
        $lexer->push('false', $parser->tokenId('value_false'));
        $lexer->push('null', $parser->tokenId('value_null'));
        $lexer->push('\s', Token::SKIP);
        // Skip comments.
        $lexer->push('\/\/[^\r\n]*', Token::SKIP);
        $lexer->push('\/\*(.|\*[^\/]|\/)*\*\/', Token::SKIP);
    }

    private function initParserRules(Parser $parser)
    {
        $this->addRule('object', 'open_brace close_brace', function (Parser $p, Stack $s) {
            $s->push([]);
        });
        $this->addRule('object', 'open_brace members comma close_brace', function () {});
        $this->addRule('object', 'open_brace members close_brace', function () {});
        $this->addRule('members', 'member', function () {});
        $this->addRule('members', 'members comma member', function (Parser $p, Stack $s) {
            $member = $s->top;
            $s->pop();
            $members = $s->top;
            $s->pop();
            $s->push(array_merge($members, $member));
        });
        $this->addRule('member', 'value_string colon value', function (Parser $p, Stack $s) {
            $value = $s->top;
            $s->pop();
            $s->push([ $this->parseString($p->sigil(0)) => $value ]);
        });

        $this->addRule('array', 'open_bracket close_bracket', function (Parser $p, Stack $s) {
            $s->push([]);
        });
        $this->addRule('array', 'open_bracket elements comma close_bracket', function () {});
        $this->addRule('array', 'open_bracket elements close_bracket', function () {});
        $this->addRule('elements', 'element', function (Parser $p, Stack $s) {
            $top = $s->top;
            $s->pop();
            $s->push([ $top ]);
        });
        $this->addRule('elements', 'elements comma element', function (Parser $p, Stack $s) {
            $element = $s->top;
            $s->pop();
            $elements = $s->top;
            $s->pop();
            $elements[] = $element;
            $s->push($elements);
        });
        $this->addRule('element', 'value', function () {});

        $this->addRule('value', 'value_string', function (Parser $p, Stack $s) {
            $s->push($this->parseString($p->sigil(0)));
        });
        $this->addRule('value', 'value_float', function (Parser $p, Stack $s) {
            $s->push((float)$p->sigil(0));
        });
        $this->addRule('value', 'value_integer', function (Parser $p, Stack $s) {
            $s->push((int)$p->sigil(0));
        });
        $this->addRule('value', 'value_true', function (Parser $p, Stack $s) {
            $s->push(true);
        });
        $this->addRule('value', 'value_false', function (Parser $p, Stack $s) {
            $s->push(false);
        });
        $this->addRule('value', 'value_null', function (Parser $p, Stack $s) {
            $s->push(null);
        });

        $this->addRule('value', 'object', function () {});
        $this->addRule('value', 'array', function () {});
    }
}
